# opensearx-ws

Meta opensearch engine

## LICENCE

Dependencies :

- fastapi : MIT License
- httpx : BSD License
- uvicorn : BSD License
