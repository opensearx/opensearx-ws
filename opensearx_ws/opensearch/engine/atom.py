"""Opensearch atom parser engine module."""
import re
from typing import List, Optional, Tuple
from xml.etree.ElementTree import Element

import httpx
from defusedxml import ElementTree as SecuredET

from opensearx_ws.opensearch.engine.base import OpensearchEngine
from opensearx_ws.opensearch.engine.helpers import harmonise_link
from opensearx_ws.opensearch.model import (
    OpensearchParsedResponse,
    OpensearchResponseEntry,
    OpensearchResponseEntryLink,
    OpensearchResponseHeader,
)


class AtomOpensearchEngine(OpensearchEngine):
    """Opensearch atom parser engine class."""

    # namespace to parse XML
    namespaces = {
        'feed': 'http://www.w3.org/2005/Atom',
        'opensearch': 'http://a9.com/-/spec/opensearch/1.1/',
        'geo': 'http://a9.com/-/opensearch/extensions/geo/1.0/',
        'time': 'http://a9.com/-/opensearch/extensions/time/1.0/',
        'georss': 'http://www.georss.org/georss/10',
        'dc': 'http://purl.org/dc/elements/1.1',
        'gml': 'http://www.opengis.net/gml',
        'cwic': 'http://cwic.wgiss.ceos.org/opensearch/extensions/1.0/',
    }

    def get_text(self, elt: Element, name: str) -> Optional[str]:
        """Retrieve text from XML element.

        Args:
            elt: root XMl element
            name: name of the element to find

        Returns:
            Text of the element
        """
        sub_elt = elt.find(name, namespaces=self.namespaces)
        return sub_elt.text if sub_elt is not None and sub_elt.text.strip() else None

    def get_children(self, elt: Element, name: str) -> List[Element]:
        """Retrieve XML children elements by name.

        Args:
            elt: root XMl element
            name: name of the elements to find

        Returns:
            List of XML element
        """
        return elt.findall(name, namespaces=self.namespaces)

    def process_response(self, http_response: httpx.Response, protocol: str) -> OpensearchParsedResponse:
        """Retrieve and parse Opensearch response.

        Args:
            http_response: HTTP response instance
            protocol: keep only the expected protocols

        Returns:
            Opensearch header response instance
        """
        feed_elt = SecuredET.fromstring(http_response.content.decode('utf8'))
        header = self._parse_header(feed_elt)
        entries = [
            self._parse_entry(entry, protocol) for entry in self.get_children(feed_elt, 'feed:entry')
        ]
        return OpensearchParsedResponse(header=header, entries=entries)

    def _parse_header(self, feed_elt: Element) -> OpensearchResponseHeader:
        """Parse header XML element.

        Args:
            feed_elt: root XMl element

        Returns:
            Opensearch header response instance
        """
        return OpensearchResponseHeader(
            title=self.get_text(feed_elt, 'feed:title'),
            id=self.get_text(feed_elt, 'feed:id'),
            updated=self.get_text(feed_elt, 'feed:updated'),
            total_results=self.get_text(feed_elt, 'opensearch:totalResults'),
            start_index=self.get_text(feed_elt, 'opensearch:startIndex'),
            items_per_page=self.get_text(feed_elt, 'opensearch:itemsPerPage'),
        )

    def _parse_entry(self, entry_elt: Element, protocol: str) -> OpensearchResponseEntry:
        """Parse entry XML element.

        Args:
            entry_elt: entry XMl element
            protocol: keep only expected protocol

        Returns:
            Opensearch entry response instance
        """
        opensearch_entry = self._parse_entry_metadata(entry_elt)

        filter_protocols = self.config.filter_protocols
        if protocol:
            if protocol not in self.config.filter_protocols:
                return opensearch_entry
            else:
                filter_protocols = {protocol: self.config.filter_protocols[protocol]}

        for link in self.get_children(entry_elt, 'feed:link'):
            harmonised_link = harmonise_link(self._parse_link(link), filter_protocols)
            if harmonised_link:
                opensearch_entry.links.append(harmonised_link)

        return opensearch_entry

    def _parse_entry_metadata(self, entry_elt: Element) -> OpensearchResponseEntry:
        """Parse XML entry metadata.

        Args:
            entry_elt: XML element

        Returns:
            Opensearch entry response instance
        """
        return OpensearchResponseEntry(
            id=self.get_text(entry_elt, 'feed:id'),
            title=self.get_text(entry_elt, 'feed:title'),
            updated=self.get_text(entry_elt, 'feed:updated'),
            summary=self.get_text(entry_elt, 'feed:summary'),
            source=self.config.source_name,
            dc_date=self.get_text(entry_elt, 'dc:date'),
            geo_box=self.get_text(entry_elt, 'georss:box'),
            geo_where=self.get_text(entry_elt, 'georss:where'),
            geo_polygon=self.get_text(entry_elt, 'georss:polygon'),
            geo_line=self.get_text(entry_elt, 'georss:line'),
        )

    def _parse_link(self, link_elt: Element) -> OpensearchResponseEntryLink:
        """Parse link XML element.

        Args:
            link_elt: link XMl element

        Returns:
            Opensearch link instance
        """
        return OpensearchResponseEntryLink(
            title=link_elt.get('title'),
            href=link_elt.get('href'),
            rel=link_elt.get('rel'),
            type=link_elt.get('type'),
        )
