"""Opensearch engines module."""
from __future__ import annotations

import json
import logging
from abc import ABC, abstractmethod
from typing import Any, Dict, Optional

import httpx
from fastapi import HTTPException
from pydantic import ValidationError
from starlette import status

from opensearx_ws.model import OpensearxQueryParameters
from opensearx_ws.opensearch.helpers import build_search_url, format_error_message
from opensearx_ws.opensearch.model import (
    OpensearchEngineConfig,
    OpensearchParsedResponse,
    OpensearchQuery,
    OpensearchResponse,
)

DEFAULT_DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'  # noqa: WPS323


async def request_hook(request: httpx.Request) -> None:
    """Print information when request is executed.

    Args:
        request: HTTP request
    """
    print('Request event hook: {0} {1} - Waiting for response', request.method, request.url)


async def response_hook(response: httpx.Response) -> None:
    """Print information when the response is retrieved.

    Args:
        response: HTTP response
    """
    request = response.request
    print('Response event hook: {0} {1} - Status {2}', request.method, request.url, response.status_code)


class OpensearchEngine(ABC):
    """Opensearch engine abstract class."""

    def __init__(self, config: OpensearchEngineConfig):
        """Initialize the opensearch engine instance.

        Args:
            config: opensearch engine configuration
        """
        self._log = logging.getLogger(__name__)
        self.config = config

    def to_dict(self) -> Dict:
        """Expose main attributes as a dictionary.

        Returns:
            dictionary
        """
        return {
            'engine_class': self.__class__.__name__,
            'engine_module': self.__class__.__module__,
            'engine_config': json.loads(self.config.json()),
        }

    def is_dataset_available(self, dataset: str):
        """Check if the dataset available for this engine.

        Args:
            dataset: dataset name

        Returns:
            True if the dataset is available
        """
        return dataset in self.config.datasets_mapping

    async def request(self, params: OpensearxQueryParameters, can_raise: bool = False) -> Optional[OpensearchResponse]:
        """Execute request and parse response.

        Args:
            params: Opensearx query parameters
            can_raise: can raise Exception if an error handle

        Returns:
            Opensearch response instance

        Raises:
            HTTPException: dataset not available
        """
        opensearch_response = OpensearchResponse()

        try:
            # check if datataset is available
            if not self.is_dataset_available(params.datasetId):
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail='The dataset {0} is not available for source {1}.'.format(
                        params.datasetId,
                        self.config.source_name,
                    ),
                )

            # build url
            effective_params = self.prepare_query_parameters(params)
            url = build_search_url(self.config.endpoint, effective_params)

            # initialize response
            opensearch_response.query = OpensearchQuery(params=effective_params, url=url)

            # execute url
            async with httpx.AsyncClient(
                event_hooks={'request': [request_hook], 'response': [response_hook]},
            ) as client:

                # check if params define protocol filter
                if params.protocol and params.protocol not in self.config.filter_protocols:
                    raise HTTPException(
                        status_code=status.HTTP_404_NOT_FOUND,
                        detail='The protocol {0} is not available for source {1}.'.format(
                            params.protocol,
                            self.config.source_name,
                        ),
                    )

                parsed_response = self.process_response(
                    await client.get(url=url, timeout=self.config.timeout),
                    params.protocol,
                )
                opensearch_response.header = parsed_response.header
                opensearch_response.entries = parsed_response.entries
        except httpx.TimeoutException:
            opensearch_response.errors.append('Timeout occurred')
        except ValidationError as error:
            opensearch_response.errors.append('Not a valid response : {0}'.format(error))
        except HTTPException as error:
            if can_raise:
                raise error
            opensearch_response.errors.append(error.detail)
        except Exception as error:
            print(error)
            opensearch_response.errors.append(format_error_message(error))
        finally:
            if opensearch_response.header is None and not opensearch_response.errors:
                opensearch_response.errors.append('Empty response')
        return opensearch_response

    def prepare_query_parameters(self, params: OpensearxQueryParameters) -> Dict[str, Any]:
        """Prepare opensearch query parameters.

        Args:
            params: Opensearx query parameters

        Returns:
            Dictionary of formatted parameters
        """
        parameters = {
            'datasetId': self.config.datasets_mapping[params.datasetId],
            'count': params.count,
            'startPage': params.startPage,
        }

        if params.timeStart:
            parameters['timeStart'] = params.timeStart.strftime(DEFAULT_DATE_FORMAT)

        if params.timeEnd:
            parameters['timeEnd'] = params.timeEnd.strftime(DEFAULT_DATE_FORMAT)

        if params.geoBox:
            parameters['geoBox'] = params.geoBox

        return parameters

    @abstractmethod
    def process_response(self, http_response: httpx.Response, protocol: str) -> OpensearchParsedResponse:
        """Retrieve and parse Opensearch response.

        Args:
            http_response: HTTP response instance
            protocol: keep only the expected protocols

        Returns:
            Opensearch header response instance
        """
        raise NotImplementedError()
