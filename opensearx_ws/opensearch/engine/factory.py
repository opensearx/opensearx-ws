"""Opensearch engine factory module."""
import importlib
from typing import Dict, List

from opensearx_ws.opensearch.engine.base import OpensearchEngine
from opensearx_ws.opensearch.model import OpensearchEngineFactoryConfig


def opensearch_engine_factory(engine_conf: OpensearchEngineFactoryConfig) -> OpensearchEngine:
    """Opensearch engine factory.

    Args:
        engine_conf: opensearch engine configuration

    Returns:
        Opensearch engine instance.
    """
    module = importlib.import_module('{0}.{1}'.format(
        engine_conf.engine_module.lower(),
        engine_conf.engine_class.lower(),
    ))
    class_ = getattr(module, '{0}OpensearchEngine'.format(engine_conf.engine_class.capitalize()))
    return class_(engine_conf.engine_config)


def opensearch_engines_as_dict(opensearch_engines: List[OpensearchEngineFactoryConfig]) -> Dict[str, OpensearchEngine]:
    """Transform a list of opensearch engines into a dictionary.

    Args:
        opensearch_engines: list of opensearch engines

    Returns:
        Dictionary of opensearch engines
    """
    return {
        opensearch_engine.engine_config.source_name: opensearch_engine_factory(opensearch_engine)
        for opensearch_engine in opensearch_engines
    }
