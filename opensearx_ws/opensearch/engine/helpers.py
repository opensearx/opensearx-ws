"""Opensearch engines helpers module."""
import asyncio
import re
from typing import Dict, List

from opensearx_ws.model import OpensearxQueryParameters
from opensearx_ws.opensearch.engine.base import OpensearchEngine
from opensearx_ws.opensearch.model import OpensearchResponse, OpensearchResponseEntryLink


async def request_engines(
    opensearch_engines: Dict[str, OpensearchEngine],
    params: OpensearxQueryParameters,
) -> List[OpensearchResponse]:
    """Execute request in parallel for each opensearch engines.

    Args:
        opensearch_engines: opensearch engines
        params: opensearx query parameters

    Returns:
        List of opensearch responses

    Raises:
        AttributeError: if engine does not exist
    """
    # filter engines
    if params.source:
        if params.source not in opensearch_engines:
            raise AttributeError('Engine `{0}` does not exists'.format(params.source))
        filtered_engines = {params.source: opensearch_engines[params.source]}
    else:
        filtered_engines = opensearch_engines

    # prepare task (i.e. coroutines)
    can_raise = len(filtered_engines) == 1
    print(can_raise)
    tasks = [asyncio.create_task(engine.request(params, can_raise)) for engine in filtered_engines.values()]

    # execute taks in parallel and retrieve a list of results
    responses = await asyncio.gather(*tasks)
    return list(responses)


def harmonise_link(link: OpensearchResponseEntryLink, filter_protocols: Dict[str, Dict[str, List]] = None):
    # no filters
    if not filter_protocols:
        return link

    #
    for filter_protocol, filters in filter_protocols.items():
        for filter_field, filters in filters.items():
            if link.title and 'title' == filter_field:
                for filter_expr in filters:
                    if re.match(filter_expr, link.title):
                        link.title = filter_protocol
                        return link

            if 'href' == filter_field:
                for filter_expr in filters:
                    if re.match(filter_expr, link.href):
                        link.title = filter_protocol
                        return link

    return None
