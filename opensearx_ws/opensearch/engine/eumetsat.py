"""PO.DAAC opensearch engine module."""
from typing import Any, Dict

from opensearx_ws.model import OpensearxQueryParameters
from opensearx_ws.opensearch.engine.atom import AtomOpensearchEngine
from opensearx_ws.opensearch.engine.base import DEFAULT_DATE_FORMAT


class EumetsatOpensearchEngine(AtomOpensearchEngine):
    """PO.DAAC opensearch engine class."""

    namespaces = {
        'feed': 'http://www.w3.org/2005/Atom',
        'opensearch': 'http://a9.com/-/spec/opensearch/1.1/',
        'geo': 'http://a9.com/-/opensearch/extensions/geo/1.0/',
        'time': 'http://a9.com/-/opensearch/extensions/time/1.0/',
        'georss': 'http://www.georss.org/georss',
        'dc': 'http://purl.org/dc/elements/1.1/',
        'gml': 'http://www.opengis.net/gml',
        'cwic': 'http://cwic.wgiss.ceos.org/opensearch/extensions/1.0/',
        'echo': 'https://cmr.earthdata.nasa.gov/search/site/docs/search/api.html#atom',
        'esipdiscovery': 'http://commons.esipfed.org/ns/discovery/1.2/',
        'eo': 'http://a9.com/-/opensearch/extensions/eo/1.0/',
    }

    def prepare_query_parameters(self, params: OpensearxQueryParameters) -> Dict[str, Any]:
        """Prepare opensearch query parameters.

        Args:
            params: Opensearx query parameters

        Returns:
            Dictionary of formatted parameters
        """
        mapping = self.config.datasets_mapping[params.datasetId].split('@')

        parameters = {
            'pi': mapping[0],
            'sat': mapping[1],
            'timeliness': mapping[2],
            'c': params.count,
            'pw': (params.startPage or 0),
        }

        if params.timeStart:
            parameters['dtstart'] = params.timeStart.strftime(DEFAULT_DATE_FORMAT)

        if params.timeEnd:
            parameters['dtend'] = params.timeEnd.strftime(DEFAULT_DATE_FORMAT)

        if params.geoBox:
            parameters['bbox'] = params.geoBox

        return parameters
