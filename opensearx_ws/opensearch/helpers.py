"""Opensearch utilities module."""

from typing import Dict
from urllib.parse import parse_qs, quote, urlencode, urlsplit, urlunsplit


def build_search_url(endpoint: str, params: Dict[str, str]) -> str:
    """Build opensearch url.

    Args:
        endpoint: opensearch root url
        params: query parameters

    Returns:
        url to execute
    """
    splitted_url = list(urlsplit(endpoint))
    query_params = parse_qs(splitted_url[3])
    for key, value in params.items():
        query_params[key] = value
    splitted_url[3] = urlencode(query_params, doseq=False, quote_via=quote)
    return urlunsplit(splitted_url)


def format_error_message(error: Exception):
    """Format error message.

    Args:
        error: exception instance

    Returns:
        Formatted error message
    """
    msg = str(error).strip()
    if not msg:
        return 'Empty response'
    return msg
