"""Opensearch response merger module."""
from typing import List

from starlette.datastructures import URL

from opensearx_ws.model import OpensearxQueryParameters
from opensearx_ws.opensearch.model import (
    OpensearchQuery,
    OpensearchResponse,
    OpensearchResponseEntry,
    OpensearchResponseHeader,
)


def merge_opensearch_responses(
    query_url: URL,
    responses: List[OpensearchResponse],
    params: OpensearxQueryParameters,
    engine_names: List,
) -> OpensearchResponse:
    """Merge opensearch response in a single one.

    Args:
        query_url: opensearx url
        responses: list of opensearch response
        params: opensearx parameters
        engine_names: list of engine names

    Returns:
        Opensearch response
    """
    computed_total_results = 0
    merged_entries: List[OpensearchResponseEntry] = []
    errors = []

    for idx, response in enumerate(responses):
        errors.extend(format_errors(engine_names[idx], errors))
        if response is None or response.header is None:
            continue

        computed_total_results += response.header.total_results
        merged_entries.extend(response.entries)

    return OpensearchResponse(
        query=OpensearchQuery(url=str(query_url), params=params),
        errors=errors,
        header=OpensearchResponseHeader(
            id=str(query_url),
            title='opensearx results',
            total_results=computed_total_results,
            start_index=params.startPage,
            items_per_page=params.count,
        ),
        entries=merged_entries,
    )


def format_errors(engine_name: str, errors: List[str]) -> List[str]:
    """Format opensearch errors.

    Args:
        engine_name: opensearch engine name
        errors: opensearch engine errors

    Returns:
        formatted errors
    """
    return ['{0} : {1}'.format(engine_name, error) for error in errors]
