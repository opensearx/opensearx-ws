"""Opensearch model module."""

from datetime import datetime
from enum import Enum
from typing import Any, Dict, List

from pydantic import HttpUrl
from pydantic.main import BaseModel


class OpensearchEngineConfig(BaseModel):
    """Opensearch engine configuration."""

    name: str
    source_name: str
    documentation_url: HttpUrl = None
    endpoint: HttpUrl
    timeout: float = 30.0
    datasets_mapping: Dict
    filter_protocols: Dict[str, Dict[str, List]] = None


class OpensearchEngineFactoryConfig(BaseModel):
    """Opensearch engine factory configuration."""

    engine_class: str = 'atom'
    engine_module: str = 'opensearx_ws.opensearch.engine'
    engine_config: OpensearchEngineConfig


class OpensearchResponseFormat(str, Enum):
    """Opensearch format enum."""

    atom = 'atom'
    json = 'json'
    raw = 'raw'


class OpensearchResponseEntryLink(BaseModel):
    """Opensearch entry link response model."""

    title: str = None
    rel: str
    type: str = None
    href: str


class OpensearchResponseEntry(BaseModel):
    """Opensearch entry response model."""

    id: str = None
    title: str = None
    summary: str = None
    updated: datetime = None
    dc_date: str = None
    geo_box: str = None
    geo_where: str = None
    geo_polygon: str = None
    geo_line: str = None
    links: List[OpensearchResponseEntryLink] = []
    source: str = None


class OpensearchResponseHeader(BaseModel):
    """Opensearch header response model."""

    id: str
    title: str
    updated: datetime = datetime.utcnow()
    total_results: int = 0
    start_index: int = 0
    items_per_page: int = 0


class OpensearchQuery(BaseModel):
    """Opensearch query model."""

    url: str
    params: Dict[str, Any]


class OpensearchResponse(BaseModel):
    """Opensearch response model."""

    query: OpensearchQuery = None
    errors: List[str] = []
    header: OpensearchResponseHeader = None
    entries: List[OpensearchResponseEntry] = []


class OpensearchParsedResponse(BaseModel):
    """Opensearch parsed response model."""

    header: OpensearchResponseHeader
    entries: List[OpensearchResponseEntry] = []
