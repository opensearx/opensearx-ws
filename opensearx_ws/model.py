"""Opensearx model module."""
from datetime import datetime

from pydantic import BaseModel, conint


class OpensearxQueryParameters(BaseModel):
    """Opensearx query parameters class."""

    datasetId: str
    startPage: int = 0
    count: conint(le=1000) = 2000
    timeStart: datetime = None
    timeEnd: datetime = None
    geoBox: str = None
    protocol: str = None
    source: str = None
