"""Opensearx webservice.

Execute requests on opensearch engines and merge the results.
"""
import os
from typing import List

import uvicorn
from fastapi import Depends, FastAPI
from fastapi.responses import ORJSONResponse
from starlette.requests import Request
from starlette.responses import HTMLResponse, JSONResponse, Response
from starlette.staticfiles import StaticFiles

from opensearx_ws.conf import config, security
from opensearx_ws.model import OpensearxQueryParameters
from opensearx_ws.opensearch import merger, model
from opensearx_ws.opensearch.engine.helpers import request_engines

# either globally or in a router
app = FastAPI(
    endpoint=config.settings.context_path,
    title='OpensearX',
    description='Execute requests on opensearch engines and merge the results.',
    version=config.settings.version,
)

app.mount('/static', StaticFiles(directory=str(config.settings.static_path)), name='static')


@app.get('/datasets')
def datasets() -> JSONResponse:
    """List available datasets.

    Returns:
        JSONResponse: Available datasets
    """
    return JSONResponse(config.settings.get_datasets_config().datasets)


@app.get('/engines')
def engines() -> JSONResponse:
    """Display the configuration.

    Returns:
        JSONResponse: Available engines
    """
    return JSONResponse({key: value.to_dict() for key, value in config.opensearch_engines.items()})


@app.get('/granules', response_model=model.OpensearchResponse)
@app.get('/granules.{response_fmt}', response_model=model.OpensearchResponse)
async def granules(
    request: Request,
    response_fmt: model.OpensearchResponseFormat = model.OpensearchResponseFormat.atom,
    params: OpensearxQueryParameters = Depends(),  # noqa: B008, WPS404
) -> Response:
    """Execute a request on each opensearch engine if the dataset is available.

    If the output format is `raw`, then return a list for the raw responses.
    Otherwise, the responses are merged into a single one and then is formatted
    to the specified format (`atom`, `json`)

    Args:
        request: HTTP request
        response_fmt: output format
        params: search parameters

    Returns:
        Response: different formats (JSON, XML)
    """
    # execute tasks in parallel and retrieve a list of results
    responses = await request_engines(config.opensearch_engines, params)

    # if response is `raw` -> return a dict<engine_name, response> as a JSON document
    if response_fmt == model.OpensearchResponseFormat.raw:
        return raw_responses(responses)

    # else, we merge response into a single one
    merged_response = merger.merge_opensearch_responses(request.url, responses, params, config.opensearch_engine_names)

    # if format is `json` -> return the response as a JSON document
    if response_fmt == model.OpensearchResponseFormat.json:
        return ORJSONResponse(merged_response.dict())

    # if format is `atom` -> return the response as an ATOM document using a jinja2 template
    if response_fmt == model.OpensearchResponseFormat.atom:
        return config.j2_templates.TemplateResponse(
            name='granules.atom',
            context={'request': request, 'response': merged_response},
            media_type='application/xml',
        )


def raw_responses(responses: List[model.OpensearchResponse]) -> ORJSONResponse:
    """Format raw responses.

    Args:
        responses: opensearch engine responses

    Returns:
        ORJSONResponse: responses in a JSON format
    """
    engine_responses = {}
    for idx, response in enumerate(responses):
        if response:
            engine_responses[config.opensearch_engine_names[idx]] = response.dict()
    return ORJSONResponse(engine_responses)


@app.get('/', include_in_schema=False)
def home(request: Request) -> HTMLResponse:
    """Home page (HTML).

    Args:
        request: HTTP request

    Returns:
        Content of template home.html
    """
    return config.j2_templates.TemplateResponse(
        name='home.html',
        context={
            'request': request,
            'datasets': config.settings.get_datasets_config().datasets,
            'engines': config.settings.get_datasets_config().opensearch_engines,
            'formats': model.OpensearchResponseFormat,
        },
    )


@app.get(
    '/admin/reload',
    dependencies=[Depends(security.validate_api_key)],
    include_in_schema=False,
)
def reload_datasets() -> Response:
    """Reload datasets configuration endpoint.

    Returns:
        HTTP response
    """
    config.get_settings.cache_clear()
    config.get_settings()
    return JSONResponse({'detail': 'Configuration updated.'})


if __name__ == '__main__':
    os.environ['datasets_config_path'] = '../examples/datasets.json'
    uvicorn.run('opensearx_ws.main:app', host='127.0.0.1', port=8000, reload=True)  # noqa: WPS432
