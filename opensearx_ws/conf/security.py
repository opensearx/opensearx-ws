"""Application security module."""
from fastapi import HTTPException, Security
from fastapi.security import APIKeyHeader
from starlette.status import HTTP_403_FORBIDDEN

from opensearx_ws.conf.config import settings

api_key_query = APIKeyHeader(name='access_token', auto_error=False)


async def validate_api_key(
    api_key_header: str = Security(api_key_query),  # noqa: WPS404, B008
) -> None:
    """Retrieve and check the API key.

    Args:
        api_key_header: api key stored in HTTP header

    Raises:
        HTTPException: if the API key is invalid
    """
    if api_key_header == settings.api_key.get_secret_value():
        return

    raise HTTPException(
        status_code=HTTP_403_FORBIDDEN,
        detail='Could not validate credentials',
    )
