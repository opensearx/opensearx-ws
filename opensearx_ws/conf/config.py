"""Opensearx configuration module."""
import json
import logging
import secrets
from pathlib import Path
from typing import List, Dict, Any

from UltraDict import UltraDict
from pydantic import BaseModel, BaseSettings, SecretStr, validator
from pydantic.tools import lru_cache
from starlette.templating import Jinja2Templates

from opensearx_ws.conf.version import get_version
from opensearx_ws.opensearch.engine.factory import opensearch_engines_as_dict
from opensearx_ws.opensearch.model import OpensearchEngineConfig, OpensearchEngineFactoryConfig

DEFAULT_API_KEY_LENGTH = 16

logger = logging.getLogger(__name__)


def generate_api_key() -> str:
    """Generate random API key.

    Returns:
        API key
    """
    secret = secrets.token_hex(DEFAULT_API_KEY_LENGTH)
    print('Generated API key : {0}'.format(secret))
    return secret


class DatasetConfiguration(BaseModel):
    """Dataset configuration model."""

    datasets: List[str] = [
        'AVHRR_SST_METOP_A-OSISAF-L2P-v1.0',
        'AVHRR_SST_METOP_B-OSISAF-L2P-v1.0',
        'AVHRR_SST_METOP_C-OSISAF-L2P-v1.0',
    ]
    opensearch_engines: List[OpensearchEngineFactoryConfig] = [
        OpensearchEngineFactoryConfig(
            engine_class='atom',
            engine_config=OpensearchEngineConfig(
                name='OSISAF Opensearch service',
                source_name='OSISAF',
                documentation_url='https://opensearch.ifremer.fr',
                endpoint='https://opensearch.ifremer.fr/granules.atom',
                timeout=60.0,
                datasets_mapping={
                    'AVHRR_SST_METOP_A-OSISAF-L2P-v1.0': 'avhrr_sst_metop_a-osisaf-l2p-v1.0',
                    'AVHRR_SST_METOP_B-OSISAF-L2P-v1.0': 'avhrr_sst_metop_b-osisaf-l2p-v1.0',
                    'AVHRR_SST_METOP_C-OSISAF-L2P-v1.0': 'avhrr_sst_metop_c-osisaf-l2p-v1.0',
                },
                filter_protocols={
                    'FTP': {
                        'title': ['FTP'],
                    },
                    'HTTPS': {
                        'title': ['HTTPS'],
                    }
                },
            ),
        ),
    ]


class Settings(BaseSettings):
    """Opensearx configuration class."""

    context_path: str = ''
    version: str = get_version()
    api_key: SecretStr = None
    templates_path: Path = Path('templates')
    static_path: Path = Path('static')
    datasets_config_path: Path = Path('/app/data/datasets.json')
    datasets_config_dict: Dict[str, Any] = None

    def get_datasets_config(self) -> DatasetConfiguration:
        """ Parse ultradict shared across unicorn workers to retrieve datasets configuration.

        Returns:
            The datasets configuration
        """
        return DatasetConfiguration.parse_obj(self.datasets_config_dict)

    @validator('api_key')
    def init_api_key(cls, value: SecretStr = None):  # noqa: N805
        """Initialize API key value.

        Args:
            value: expected api key

        Returns:
            API key generated or set
        """
        if value is None:
            return SecretStr(generate_api_key())
        return value

    def load_datasets_config(self) -> None:
        """Load datasets configuration."""
        self.datasets_config_dict = UltraDict(name='datasets_config_dict')
        conf_dict = dict()
        if self.datasets_config_path is None or not self.datasets_config_path.is_file():
            print('Load default datasets configuration')
            conf_dict = DatasetConfiguration().dict()
        else:
            print('Load datasets configuration from file : {0}'.format(self.datasets_config_path))
            try:
                with open(self.datasets_config_path) as conf_file:
                    conf_dict = json.load(conf_file)
            except Exception as error:
                print('Error while loading file : {0}'.format(self.datasets_config_path))
        # update ultradict shared across unicorn workers, see issue #20
        self.datasets_config_dict.data = {}
        self.datasets_config_dict.update(conf_dict)


@lru_cache()
def get_settings() -> Settings:
    """Load settings and cache it.

    Returns:
        Opensearx settings
    """
    app_settings = Settings()
    app_settings.load_datasets_config()
    return app_settings


settings = get_settings()

# jinja2 templates utility
j2_templates = Jinja2Templates(directory=settings.templates_path)

# opensearch engines instances
opensearch_engines = opensearch_engines_as_dict(settings.get_datasets_config().opensearch_engines)
opensearch_engine_names = list(opensearch_engines.keys())
