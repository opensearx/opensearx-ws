"""Retrieve package version."""
import subprocess  # noqa: S404
from typing import Optional

try:
    import pkg_resources  # noqa: WPS433
except ModuleNotFoundError:
    pkg_resources = None  # noqa: WPS440


def get_version_from_package() -> Optional[str]:
    """Retrieve package version from deployed package metadata.

    Returns:
        str: package version
    """
    if not pkg_resources:
        return None

    try:
        return pkg_resources.get_distribution('cops_dashboard').version
    except pkg_resources.DistributionNotFound:
        return None


def get_version_from_git(exact_match: bool = True) -> Optional[str]:
    """Retrieve package version from git metadata.

    Args:
        exact_match: if True, search for tag attached to the current commit

    Returns:
        str: package version
    """
    cmd = ['git', 'describe', '--tags', 'HEAD']
    if exact_match:
        cmd.insert(-1, '--exact-match')

    try:
        return subprocess.check_output(cmd).strip().decode()  # noqa: S603
    except (FileNotFoundError, subprocess.CalledProcessError):
        return None


def get_version() -> str:
    """Retrieve package version.

    Returns:
        str: package version
    """
    default_version = '0.0.0'
    version = get_version_from_package()
    if version and version != default_version:
        return version

    version = get_version_from_git(exact_match=True)
    if version and version != default_version:
        return version

    version = get_version_from_git(exact_match=False)
    if version:
        return version

    return default_version
