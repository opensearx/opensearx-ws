"""Test opensearch response merging."""
from typing import Dict

import pytest
from starlette.datastructures import URL

from opensearx_ws.model import OpensearxQueryParameters
from opensearx_ws.opensearch.engine.base import OpensearchEngine
from opensearx_ws.opensearch.engine.helpers import request_engines
from opensearx_ws.opensearch.merger import merge_opensearch_responses


@pytest.fixture()
def fake_url() -> URL:
    return URL('http://fake.url.com')


@pytest.mark.asyncio
async def test_merge(
    opensearch_engines: Dict[str, OpensearchEngine],
    osi200_request: OpensearxQueryParameters,
    fake_url: URL,
):
    responses = await request_engines(opensearch_engines, osi200_request)
    merged_response = merge_opensearch_responses(
        fake_url,
        responses,
        osi200_request,
        list(opensearch_engines.keys()),
    )
    assert len(merged_response.entries) == 20


@pytest.mark.asyncio
async def test_merge_with_filter_source(
    opensearch_engines: Dict[str, OpensearchEngine],
    osi200_request: OpensearxQueryParameters,
    fake_url: URL,
):
    osi200_request.source = 'PODAAC'
    responses = await request_engines(opensearch_engines, osi200_request)
    merged_response = merge_opensearch_responses(
        fake_url,
        responses,
        osi200_request,
        list(opensearch_engines.keys()),
    )
    assert len(merged_response.entries) == 10
    assert len(merged_response.entries[0].links) == 1
    assert merged_response.entries[0].links[0].title == 'The HTTP location for the granule.'


@pytest.mark.asyncio
async def test_merge_with_filter_protocol(
    opensearch_engines: Dict[str, OpensearchEngine],
    osi200_request: OpensearxQueryParameters,
    fake_url: URL,
):
    osi200_request.protocol = 'FTP'
    responses = await request_engines(opensearch_engines, osi200_request)
    merged_response = merge_opensearch_responses(
        fake_url,
        responses,
        osi200_request,
        list(opensearch_engines.keys()),
    )
    assert len(merged_response.entries) == 10
    assert len(merged_response.entries[0].links) == 1
    assert merged_response.entries[0].links[0].title == 'FTP'
