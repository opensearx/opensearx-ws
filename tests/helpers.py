"""Tests utilities."""
from pathlib import Path


def read_file(filepath: Path) -> str:
    """Read file content.

    Args:
        filepath: file path

    Returns:
        file content
    """
    with open(str(filepath)) as fd:
        return fd.read()
