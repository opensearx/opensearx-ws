"""Test opensearch engines module."""
import json

import pytest

from opensearx_ws.model import OpensearxQueryParameters
from opensearx_ws.opensearch.engine.base import OpensearchEngine
from opensearx_ws.opensearch.model import OpensearchResponse


def test_osisaf_engine(osisaf_engine: OpensearchEngine):
    generated = json.dumps(osisaf_engine.to_dict(), sort_keys=True)
    expected = json.dumps(
        {
            'engine_class': 'AtomOpensearchEngine',
            'engine_module': 'opensearx_ws.opensearch.engine.atom',
            'engine_config': {
                'name': 'OSISAF Opensearch service',
                'source_name': 'OSISAF',
                'documentation_url': 'https://opensearch.ifremer.fr',
                'endpoint': 'https://opensearch.ifremer.fr/granules.atom',
                'timeout': 30.0,
                'datasets_mapping': {
                    'OSI-200': 'avhrr_sst_metop_a-osisaf-l2p-v1.0',
                    'OSI-201': 'avhrr_sst_metop_b-osisaf-l2p-v1.0',
                    'OSI-202': 'avhrr_sst_metop_c-osisaf-l2p-v1.0',
                },
                'filter_protocols': {
                    'FTP': {'title': ['FTP']},
                    'HTTPS': {'title': ['HTTPS']},
                },
            },
        },
        sort_keys=True,
    )
    assert generated == expected
    assert osisaf_engine.is_dataset_available('OSI-200') is True
    assert osisaf_engine.is_dataset_available('OSI-204') is False


def test_podaac_engine(podaac_engine: OpensearchEngine):
    generated = json.dumps(podaac_engine.to_dict(), sort_keys=True)
    expected = json.dumps(
        {
            'engine_class': 'PodaacOpensearchEngine',
            'engine_module': 'opensearx_ws.opensearch.engine.podaac',
            'engine_config': {
                'name': 'PO.DAAC Opensearch service',
                'source_name': 'PODAAC',
                'documentation_url': 'https://cmr.earthdata.nasa.gov/opensearch',
                'endpoint': 'https://cmr.earthdata.nasa.gov/opensearch/granules.atom',
                'timeout': 30.0,
                'datasets_mapping': {
                    'OSI-200': 'C1664387028-PODAAC',
                    'OSI-201': 'C1664387108-PODAAC',
                },
                'filter_protocols': {
                    'HTTPS': {
                        'title': [
                            'The HTTP location for the granule.',
                            'The base directory location for the granule.',
                        ],
                    },
                },
            },
        },
        sort_keys=True,
    )
    assert generated == expected
    assert podaac_engine.is_dataset_available('OSI-200') is True
    assert podaac_engine.is_dataset_available('OSI-202') is False


@pytest.mark.asyncio
async def test_podaac_engine_request(
    podaac_engine: OpensearchEngine,
    osi200_request: OpensearxQueryParameters,
    osi202_request: OpensearxQueryParameters,
):
    osi200_response: OpensearchResponse = await podaac_engine.request(osi200_request)
    assert len(osi200_response.entries) == 10
    assert len(osi200_response.entries[0].links) == 1

    assert await podaac_engine.request(osi202_request) == OpensearchResponse(
        query=None,
        errors=['The dataset OSI-202 is not available for source PODAAC.'],
        header=None,
        entries=[],
    )


@pytest.mark.asyncio
async def test_osisaf_engine_osi200_request(
    osisaf_engine: OpensearchEngine,
    osi200_request: OpensearxQueryParameters,
    osi202_request: OpensearxQueryParameters,
):
    osi200_response: OpensearchResponse = await osisaf_engine.request(osi200_request)
    assert len(osi200_response.entries) == 10
    assert len(osi200_response.entries[0].links) == 2

    osi202_response: OpensearchResponse = await osisaf_engine.request(osi202_request)
    assert len(osi202_response.entries) == 10
    assert len(osi202_response.entries[0].links) == 2


@pytest.mark.asyncio
async def test_osisaf_engine_filter_protocol_request(
    osisaf_engine: OpensearchEngine,
    osi200_request: OpensearxQueryParameters,
):
    osi200_request.protocol = 'FTP'
    osi200_response: OpensearchResponse = await osisaf_engine.request(osi200_request)
    assert len(osi200_response.entries[0].links) == 1
    assert osi200_response.entries[0].links[0].title == 'FTP'
