"""Pytest common fixture."""
from datetime import datetime
from pathlib import Path
from typing import Dict

from pytest import fixture

from opensearx_ws.model import OpensearxQueryParameters
from opensearx_ws.opensearch.engine.base import OpensearchEngine
from opensearx_ws.opensearch.engine.factory import opensearch_engines_as_dict
from opensearx_ws.opensearch.model import OpensearchEngineConfig, OpensearchEngineFactoryConfig
from tests.helpers import read_file


@fixture(scope='session')
def test_dir() -> Path:
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return Path(__file__).parent


@fixture(scope='session')
def resources_dir(test_dir) -> Path:
    """Path to resource dir fixture.

    Args:
        test_dir: test directory fixture

    Returns:
        Path to resource dir
    """
    return test_dir / 'resources'


@fixture
def opensearch_engines() -> Dict[str, OpensearchEngine]:
    """Create opensearch engines.

    Returns:
        List of opensearch engine
    """
    return opensearch_engines_as_dict([
        OpensearchEngineFactoryConfig(
            engine_class='atom',
            engine_config=OpensearchEngineConfig(
                name='OSISAF Opensearch service',
                source_name='OSISAF',
                documentation_url='https://opensearch.ifremer.fr',
                endpoint='https://opensearch.ifremer.fr/granules.atom',
                timeout=30.0,
                datasets_mapping={
                    'OSI-200': 'avhrr_sst_metop_a-osisaf-l2p-v1.0',
                    'OSI-201': 'avhrr_sst_metop_b-osisaf-l2p-v1.0',
                    'OSI-202': 'avhrr_sst_metop_c-osisaf-l2p-v1.0',
                },
                filter_protocols={
                    'FTP': {'title': ['FTP']},
                    'HTTPS': {'title': ['HTTPS']},
                },
            ),
        ),
        OpensearchEngineFactoryConfig(
            engine_class='podaac',
            engine_config=OpensearchEngineConfig(
                name='PO.DAAC Opensearch service',
                source_name='PODAAC',
                documentation_url='https://cmr.earthdata.nasa.gov/opensearch',
                endpoint='https://cmr.earthdata.nasa.gov/opensearch/granules.atom',
                timeout=30.0,
                datasets_mapping={
                    'OSI-200': 'C1664387028-PODAAC',
                    'OSI-201': 'C1664387108-PODAAC',
                },
                filter_protocols={
                    'HTTPS': {
                        'title': [
                            'The HTTP location for the granule.',
                            'The base directory location for the granule.',
                        ],
                    },
                },
            ),
        ),
    ])


@fixture
def osisaf_engine(opensearch_engines):
    return opensearch_engines['OSISAF']


@fixture
def podaac_engine(opensearch_engines) -> OpensearchEngine:
    return opensearch_engines['PODAAC']


@fixture
def osi200_request() -> OpensearxQueryParameters:
    return OpensearxQueryParameters(
        datasetId='OSI-200',
        startPage=0,
        count=10,
        timeStart=datetime(2000, 1, 1),
        timeEnd=datetime(2021, 12, 31),
        geoBox='-180.0,-90.0,180.0,90.0',
    )


@fixture
def osi202_request() -> OpensearxQueryParameters:
    return OpensearxQueryParameters(
        datasetId='OSI-202',
        startPage=0,
        count=10,
        timeStart=datetime(2000, 1, 1),
        timeEnd=datetime(2021, 12, 31),
        geoBox='-180.0,-90.0,180.0,90.0',
    )


@fixture(autouse=True)
def opensearch_mock(httpx_mock, resources_dir):
    """Mock opensearch engine requests.

    Args:
        httpx_mock: request mock fixture (from httpx_mock)
        resources_dir: resources dir fixture
    """
    httpx_mock.add_response(
        method='GET',
        url='https://opensearch.ifremer.fr/granules.atom?datasetId=avhrr_sst_metop_a-osisaf-l2p-v1.0'
        '&count=10&startPage=0&timeStart=2000-01-01T00%3A00%3A00Z&timeEnd=2021-12-31T00%3A00%3A00Z'
        '&geoBox=-180.0%2C-90.0%2C180.0%2C90.0',
        content=read_file(resources_dir / 'osi-200-osisaf.xml').encode(),
    )

    httpx_mock.add_response(
        method='GET',
        url='https://opensearch.ifremer.fr/granules.atom?datasetId=avhrr_sst_metop_c-osisaf-l2p-v1.0'
        '&count=10&startPage=0&timeStart=2000-01-01T00%3A00%3A00Z&timeEnd=2021-12-31T00%3A00%3A00Z'
        '&geoBox=-180.0%2C-90.0%2C180.0%2C90.0',
        content=read_file(resources_dir / 'osi-201-osisaf.xml').encode(),
    )

    httpx_mock.add_response(
        method='GET',
        url='https://cmr.earthdata.nasa.gov/opensearch/granules.atom?shortName=OSI-200'
        '&numberOfResults=10&cursor=1&commit=Search&startTime=2000-01-01T00%3A00%3A00Z'
        '&endTime=2021-12-31T00%3A00%3A00Z&boundingBox=-180.0%2C-90.0%2C180.0%2C90.0&spatial_type=bbox',
        content=read_file(resources_dir / 'osi-200-podaac.xml').encode(),
    )


@fixture
def assert_all_responses_were_requested() -> bool:
    return False
